// Challenge CH1 - Binar Backend JS Kelas 4
// Dibuat oleh Kurniawan Cristianto

// Melakukan import library yang diperlukan
const readline = require("readline");

// Membuat scanner untuk menerima input dan menghasilkan output dari console
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Data mengenai jenis-jenis kalkulasi
const dataKalkulasi = [
  // Penjumlahan :
  {
    nomor: "1",
    fungsi: () =>
      rl.question("Masukkan nilai pertama: ", (answer1) => {
        rl.question("Masukkan nilai kedua: ", (answer2) => {
          console.log(`Hasil penjumlahan adalah ${+answer1 + +answer2}`);
          rl.close();
        });
      }),
  },
  // Pengurangan :
  {
    nomor: "2",
    fungsi: () =>
      rl.question("Masukkan nilai pertama: ", (answer1) => {
        rl.question("Masukkan nilai kedua: ", (answer2) => {
          console.log(`Hasil pengurangan adalah ${+answer1 - +answer2}`);
          rl.close();
        });
      }),
  },
  // Perkalian :
  {
    nomor: "3",
    fungsi: () =>
      rl.question("Masukkan nilai pertama: ", (answer1) => {
        rl.question("Masukkan nilai kedua: ", (answer2) => {
          console.log(`Hasil perkalian adalah ${+answer1 * +answer2}`);
          rl.close();
        });
      }),
  },
  // Pembagian :
  {
    nomor: "4",
    fungsi: () => {
      const masukkanNilaiPembagi = (answer1) =>
        rl.question("Masukkan nilai pembagi: ", (answer2) => {
          if (+answer2 === 0) {
            console.log("Nilai pembagi tidak boleh nol");
            return masukkanNilaiPembagi(answer1);
          }
          console.log(`Hasil pembagian adalah ${+answer1 / +answer2}`);
          rl.close();
        });
      rl.question("Masukkan nilai yang akan dibagi: ", (answer1) => {
        masukkanNilaiPembagi(answer1);
      });
    },
  },
  // Akar :
  {
    nomor: "5",
    fungsi: () => {
      const masukkanNilaiYgDiAkar = () => {
        rl.question("Masukkan nilai yang akan di-akar: ", (answer1) => {
          if (answer1 < 0) {
            console.log("Nilai yang di-akarkan tidak boleh kurang dari nol");
            return masukkanNilaiYgDiAkar();
          }
          rl.question("Masukkan nilai faktor akar: ", (answer2) => {
            console.log(
              `Hasil akar adalah ${Math.pow(+answer1, 1 / +answer2)}`
            );
            rl.close();
          });
        });
      };
      masukkanNilaiYgDiAkar();
    },
  },
  //  Kuadrat :
  {
    nomor: "6",
    fungsi: () =>
      rl.question("Masukkan nilai yang akan di-kuadrat: ", (answer1) => {
        rl.question("Masukkan nilai faktor kuadrat: ", (answer2) => {
          console.log(`Hasil kuadrat adalah ${Math.pow(+answer1, +answer2)}`);
          rl.close();
        });
      }),
  },
  // Luas Persegi :
  {
    nomor: "7",
    fungsi: () => {
      const masukkanNilaiSisi = () =>
        rl.question("Masukkan panjang sisi persegi: ", (answer) => {
          if (answer < 0) {
            console.log("Panjang sisi persegi tidak boleh kurang dari nol");
            return masukkanNilaiSisi();
          }
          console.log(`Hasil luas persegi adalah ${answer ** 2}`);
          rl.close();
        });
      masukkanNilaiSisi();
    },
  },
  // Volume Kubus :
  {
    nomor: "8",
    fungsi: () => {
      const masukkanNilaiSisi = () =>
        rl.question("Masukkan panjang sisi kubus: ", (answer) => {
          if (answer < 0) {
            console.log("Panjang sisi kubus tidak boleh kurang dari nol");
            return masukkanNilaiSisi();
          }
          console.log(`Hasil volume kubus adalah ${answer ** 3}`);
          rl.close();
        });
      masukkanNilaiSisi();
    },
  },
  // Volume Tabung
  {
    nomor: "9",
    fungsi: () => {
      const masukkanTinggiTabung = (answer1) => {
        return rl.question("Masukkan tinggi tabung: ", (answer2) => {
          if (answer2 < 0) {
            console.log("Panjang tinggi tabung tidak boleh kurang dari nol");
            return masukkanTinggiTabung(answer1);
          }
          console.log(
            `Hasil volume tabung adalah ${Math.PI * answer1 ** 2 * answer2}`
          );
          rl.close();
        });
      };
      const masukkanJari2 = () => {
        rl.question("Masukkan panjang jari-jari alas tabung: ", (answer1) => {
          if (answer1 < 0) {
            console.log(
              "Panjang jari-jari alas tabung tidak boleh kurang dari nol"
            );
            return masukkanJari2();
          }
          return masukkanTinggiTabung(answer1);
        });
      };
      masukkanJari2();
    },
  },
];

// Fungsi utama untuk menjalankan aplikasi
const aplikasi = () =>
  rl.question(
    `Daftar jenis kalkulasi :
    (1) Penjumlahan
    (2) Pengurangan
    (3) Perkalian
    (4) Pembagian
    (5) Akar
    (6) Kuadrat
    (7) Luas Persegi
    (8) Volume Kubus
    (9) Volume Tabung
Masukkan nomor pilihan jenis kalkulasi:\n`,
    (answer) => {
      const kalkulasi = dataKalkulasi.find((v) => v.nomor === answer);
      if (!kalkulasi) {
        console.log("Jenis kalkulasi yang dipilih tidak valid\n");
        rl.close();
        aplikasi();
      }
      kalkulasi.fungsi();
    }
  );

// Fungsi listener yang dijalankan saat aplikasi selesai dipakai
rl.on("close", () => {
  console.log("Terima kasih sudah menggunakan aplikasi ini");
  process.exit(0);
});

// Menjalankan aplikasi
console.log("Challenge 1 - Program");
aplikasi();
